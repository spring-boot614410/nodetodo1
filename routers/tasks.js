const express = require('express');
const router=express.Router()
const Tasks=require('../models/taskmodel')

router.get('/',async(req,res)=>{
    try{
        const tasks=await Tasks.find();
        res.json(tasks);
}
catch(error){
    res.send(error);
}
    
})
router.get('/:id',async(req,res)=>{
    try{
        const task=await Tasks.findById(req.params.id);
        res.json(task);
}
catch(error){
    res.send(error);
}
    
})
router.post('/add', async(req, res) => {
    const task=new Tasks({
        title:req.body.title,
        status:req.body.status
    })
    
    try{
        const task1=await task.save();

        res.json(task1);
    }
    catch(error){
        res.send(error);
    }
    // const newTodo = req.body.todo;
    // todos.push(newTodo);
    // res.redirect('/');
});
router.patch('/:id',async(req,res)=>{
   
    
    try{
        const task=await Tasks.findById(req.params.id)
        task.title=req.body.title
        task.status=req.body.status
        const task1=await task.save();

        res.json(task1);
    }
    catch(error){
        res.send(error);
    }
})

router.delete('/:id', async(req, res) => {
    try{
        await Tasks.findByIdAndDelete(req.params.id)
    res.send('deleted!');
}
catch(error){
    res.send(error);
}
    // const index = req.body.index;
    // todos.splice(index, 1);
    // res.redirect('/');
});
module.exports=router

