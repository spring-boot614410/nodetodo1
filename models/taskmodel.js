const mongoose=require('mongoose')
const tasksSchema=new mongoose.Schema({
    title:{
        type:String,
        require:true
    },
    status:{
        type:Boolean,
        require:true,
        default:false
    }

})
module.exports=mongoose.model('tasks',tasksSchema)